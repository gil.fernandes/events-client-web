2024-12-18

Add Social Media Widget for Gift for the soul
Add explanation to social media widget:
The Gift for the Soul is a daily inspiration and can find more about it in these channels

Change the upload batch and retrieve the daily GFTS

Fix the missing city in the calendar

Tiles Widget needs to use the thumbnail, picture number 4

Remove comments from blog
Eventually add captcha
