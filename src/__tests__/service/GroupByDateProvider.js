export const groupByDateProvider = () => {
  return [
    {
      id: 3465419,
      eventTypeId: 3,
      name: "Raja Yoga Meditation Course - Saturday Mornings",
      subTitle: "",
      shortDescription:
        "Experience peace, inner strength and wisdom through 'yoga for the mind'.",
      descriptionText:
        "Introductory Course – leading to Intermediate level Experience peace, inner strength and wisdom through 'yoga for the mind'. Learn how to: * meditate anytime, anywhere * experience the beauty of the true self * understand the nature of consciousness * connect with the Divine * use your 8 spiritual powers A four-week course: 1 session per week, 10:00 am - 12:00 noon Saturdays, starting on Saturday 11th June 2022. Book to receive Zoom link For those who have completed the Raja Yoga Meditation Course and would like to continue to the next level, please contact us at: gchenquiries@uk.brahmakumaris.org. Individual courses are available for those who require a language other than English. We are pleased to offer courses in several Asian and European languages. Please contact us at: gchenquiries@uk.brahmakumaris.org.",
      eventDateId: 4066189,
      startIso: "2022-06-11T10:00:00",
      endIso: "2022-06-11T12:00:00",
    },
    {
      id: 4068259,
      eventTypeId: 13,
      name: "Women Inspired",
      subTitle: "Monthly Talk And Discussion",
      shortDescription:
        "A lively session for women to discuss how spirituality can support our lives and the issues we face.",
      descriptionText:
        'Topic: Being Fearless and Safe, Regardless! Women Inspired (WIN) is a monthly program supporting women to live their best lives with inner clarity. We offer an open, engaging, supportive forum that explores key issues relevant to the modern woman and reflections on how spirituality can enhance their lives and provide solutions to challenges. Supporting women to live their lives with assurance and inner clarity. In an atmosphere of understanding and respect, everyone feels welcome and has the opportunity to dialogue with the speaker who usually gives "tips and tools" on how to flourish in today\'s world. Once a month every second Saturday afternoon from 2:30 - 3.45pm. All women welcome Join on Zoom: Meeting ID: 994 8417 9924 | Passcode: 055225',
      eventDateId: 4068340,
      startIso: "2022-06-11T14:30:00",
      endIso: "2022-06-11T15:45:00",
    },
    {
      id: 4066190,
      eventTypeId: 3,
      name: "Raja Yoga Meditation Course - Weekday Mornings",
      subTitle: "",
      shortDescription:
        "Experience peace, inner strength and wisdom through 'yoga for the mind'.",
      descriptionText:
        "Course includes Introductory and Intermediate level Experience peace, inner strength and wisdom through 'yoga for the mind'. Learn how to: * meditate anytime, anywhere * experience the beauty of the true self * understand the nature of consciousness * connect with the Divine * use your 8 spiritual powers A three-week course: 3 sessions per week, 10.00am - 11.30am Mondays, Wednesdays and Fridays, (9 sessions) starting on Monday 13th June. This course is in-house | Booking essential For those who have completed the Raja Yoga Meditation Course and would like to continue to the next level, please contact us at: gchenquiries@uk.brahmakumaris.org. Individual courses are available for those who require a language other than English. We are pleased to offer courses in several Asian and European languages. Please contact us at: gchenquiries@uk.brahmakumaris.org.",
      eventDateId: 4066200,
      startIso: "2022-06-13T10:00:00",
      endIso: "2022-06-13T11:30:00",
    },
    {
      id: 4017005,
      eventTypeId: 9,
      name: "Drop IN, Power UP - Meditation for Inner Peace",
      subTitle: "",
      shortDescription:
        "An opportunity to Relax, Restore, Recharge for 15, 20, 30 mins.... to experience inner peace and strength.",
      descriptionText:
        "An opportunity to Relax, Restore, Recharge for 15, 20, 30 mins.... to experience inner peace and strength. Meditation commentaries, soft music and periods of silence. This is a new opportunity at Global Cooperation House, London NW10 7HH.",
      eventDateId: 4066336,
      startIso: "2022-06-13T10:30:00",
      endIso: "2022-06-13T11:30:00",
    },
    {
      id: 4017005,
      eventTypeId: 9,
      name: "Drop IN, Power UP - Meditation for Inner Peace",
      subTitle: "",
      shortDescription:
        "An opportunity to Relax, Restore, Recharge for 15, 20, 30 mins.... to experience inner peace and strength.",
      descriptionText:
        "An opportunity to Relax, Restore, Recharge for 15, 20, 30 mins.... to experience inner peace and strength. Meditation commentaries, soft music and periods of silence. This is a new opportunity at Global Cooperation House, London NW10 7HH.",
      eventDateId: 4066342,
      startIso: "2022-06-13T18:30:00",
      endIso: "2022-06-13T19:30:00",
    },
    {
      id: 4066398,
      eventTypeId: 2,
      name: "WANDS - Magical Tools for Self Empowerment",
      subTitle: "Weekly Talk by experienced Raja Yogi Meditators",
      shortDescription:
        "Join a weekly talk and discussion by experienced Raja Yogi meditators, to support our ongoing journey with this powerful meditation practice, connecting with The Divine. ",
      descriptionText:
        "Join a weekly talk and discussion by experienced Raja Yogi meditators, to support our ongoing journey with this powerful meditation practice, connecting with The Divine. See how with application 'magic' can happen in our thinking and way of being. Every Tuesday morning. Zoom link: https://tinyurl.com/wandsgch Meeting ID: 87434535156 | Passcode: 577203",
      eventDateId: 4066400,
      startIso: "2022-06-14T10:30:00",
      endIso: "2022-06-14T12:00:00",
    },
    {
      id: 4017005,
      eventTypeId: 9,
      name: "Drop IN, Power UP - Meditation for Inner Peace",
      subTitle: "",
      shortDescription:
        "An opportunity to Relax, Restore, Recharge for 15, 20, 30 mins.... to experience inner peace and strength.",
      descriptionText:
        "An opportunity to Relax, Restore, Recharge for 15, 20, 30 mins.... to experience inner peace and strength. Meditation commentaries, soft music and periods of silence. This is a new opportunity at Global Cooperation House, London NW10 7HH.",
      eventDateId: 4066337,
      startIso: "2022-06-15T10:30:00",
      endIso: "2022-06-15T11:30:00",
    },
    {
      id: 4082998,
      eventTypeId: 8,
      name: "Raja Yoga for the Mind with Master Yogi, Sister Jayanti",
      subTitle: "",
      shortDescription: "Honouring UN International Day of Yoga ",
      descriptionText:
        "Honouring UN International Day of Yoga This is an IN-HOUSE special event at : Global Co-operation House, 65-69 Pound Lane, London NW10 2HH (parking on the street is free from 6.30pm) or watch online : https://globalcooperationhouse.org/watchlive WATCH ONLINE",
      eventDateId: 4083001,
      startIso: "2022-06-16T19:00:00",
      endIso: "2022-06-16T20:30:00",
    },
    {
      id: 4075423,
      eventTypeId: 1,
      name: "IN HOUSE special event",
      subTitle: "",
      shortDescription: "",
      descriptionText:
        "More info soon Honouring International Day of Yoga 2022 Raja Yoga for the Mind with Master Yogi, Sister Jayanti Watch live: globalcooperationhouse.org/watchlive.",
      eventDateId: 4075425,
      startIso: "2022-06-16T19:00:00",
      endIso: "2022-06-16T20:30:00",
    },
    {
      id: 4017005,
      eventTypeId: 9,
      name: "Drop IN, Power UP - Meditation for Inner Peace",
      subTitle: "",
      shortDescription:
        "An opportunity to Relax, Restore, Recharge for 15, 20, 30 mins.... to experience inner peace and strength.",
      descriptionText:
        "An opportunity to Relax, Restore, Recharge for 15, 20, 30 mins.... to experience inner peace and strength. Meditation commentaries, soft music and periods of silence. This is a new opportunity at Global Cooperation House, London NW10 7HH.",
      eventDateId: 4066338,
      startIso: "2022-06-17T10:30:00",
      endIso: "2022-06-17T11:30:00",
    },
  ];
};
