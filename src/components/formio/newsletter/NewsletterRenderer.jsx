import React from "react";
import CheckboxRenderer from "../CheckboxRenderer";

/**
 * The counter component used to render a person counter.
 *
 * This component sets its value to the value.
 */
export default class NewsletterRenderer extends CheckboxRenderer {
  constructor(props) {
    super(props);
  }
}
