import React, { useContext } from "react";
import EventType from "../EventType";
import Venue from "../Venue";
import moment from "moment-timezone/index";
import makeModal from "../simpleModal/makeModal";
import { IncludeForm } from "../forms/FormModal";
import { useTranslation } from "../../i18n";
import WebcastButton from "../WebcastButton";
import RenderSimilarEvents from "./RenderSimilarEvents";
import { extractParameter } from "../../utils/paramExtraction";
import EventContext from "../../context/EventContext";
import useTimeFormat from "../../hooks/useTimeFormat";
import "moment/locale/de";
import {
  isSameDay,
  isSameMoment,
  renderTimeFromIso,
  timeAfterNow,
} from "../../utils/dateUtils";
import { googleCalendarLink } from "../../utils/googleCalendarUtils";
import SocialIcons from "./SocialIcons";
import { convertTagsToSet } from "../../utils/tagsAdapter";
import { EVENT_CONFIG, TAGS } from "../../context/appParams";

function renderAddToGoogleCalendar(
  event,
  date,
  t,
  useIcon = false,
  linkText = "add-google-calendar",
) {
  return (
    <a href={googleCalendarLink(event, date)} target="_blank" rel="noreferrer">
      {(!useIcon && t(linkText)) || <span>&#128276;</span>}{" "}
    </a>
  );
}

const DEFAULT_UPCOMING_LIMIT = 10;

export function RenderDate({
  date,
  currentEvent,
  timeFormat,
  useIcon = false,
  useCalendarIcon = true,
  addGoogleCalendar = true,
}) {
  const eventContext = useContext(EventContext);
  const { eventsConfig } = eventContext;

  // &dateFormat=ddd,%20MMM%20Do%20YYYY for American date
  let dateFormat = extractParameter(
    { ...eventContext },
    EVENT_CONFIG.EVENT_DATE_FORMAT,
    "ddd, Do MMM YYYY",
  );
  const tags = convertTagsToSet(currentEvent);
  if (tags.has(TAGS.US_DATE)) {
    dateFormat = "ddd, MMM Do YYYY";
  }
  const { t, langCode } = useTranslation();

  const renderEndTimeIfSameDay = () => {
    if (isSameDay(date)) {
      return " - " + renderTimeFromIso(date.endIso, langCode, timeFormat);
    }
    return "";
  };

  const startAfterNow = !!timeAfterNow(date.startIso);

  const startMoment = moment(date.startIso, "YYYY-MM-DD'T'hh:mm:ss");
  const endMoment = moment(date.endIso, "YYYY-MM-DD'T'hh:mm:ss");

  return (
    <div className="row" key={date.eventDateId}>
      <div className="calendar-date-info col-12">
        {useCalendarIcon && <span className="calendar-icon">&#x1f4c5;</span>}{" "}
        {startMoment.locale(langCode).format(`${dateFormat} ${timeFormat}`)}
        {renderEndTimeIfSameDay()}
        {!isSameMoment(startMoment, endMoment) && (
          <span>
            {" "}
            - {endMoment.locale(langCode).format(`${dateFormat} ${timeFormat}`)}
          </span>
        )}
        <span className="time-from-now">
          {" "}
          (
          {moment(date.startIso, "YYYY-MM-DD'T'hh:mm:ss")
            .locale(langCode)
            .fromNow()}
          )
        </span>
      </div>
      <div className="col-12 text-right text-nowrap">
        {!!eventsConfig.showGoogleCalendarIcon
          ? addGoogleCalendar &&
            startAfterNow &&
            renderAddToGoogleCalendar(currentEvent, date, t, useIcon)
          : currentEvent.locality}
      </div>
    </div>
  );
}

function RenderUpcomingDates({
  dateList,
  currentEvent,
  isIntermediate = false,
}) {
  const { t } = useTranslation();
  if (dateList.length === 0 && !isIntermediate) {
    dateList.push({
      eventDateId: currentEvent.eventDateId,
      endIso: currentEvent.endIso,
      startIso: currentEvent.startIso,
    });
  }
  const eventContext = useContext(EventContext);
  const timeFormat = useTimeFormat();
  moment.locale(extractParameter({ ...eventContext }, "language", "en-US"));
  const upcomingDateLimit =
    extractParameter({ ...eventContext }, "upcomingDateLimit") ||
    DEFAULT_UPCOMING_LIMIT;
  if (upcomingDateLimit && Array.isArray(dateList) && !isIntermediate) {
    dateList = dateList.slice(0, upcomingDateLimit);
  }
  if (Array.isArray(dateList) && dateList.length === 0 && isIntermediate) {
    return <></>;
  }
  return (
    <>
      <h4 className={`upcoming-dates mt-2 ${isIntermediate && "ml-4"}`}>
        {isIntermediate ? t("other-sessions") : t("upcoming-dates")}
      </h4>
      <div
        className={`card card-body bg-light ${isIntermediate && "ml-4 text-muted"}`}
      >
        {Array.isArray(dateList)
          ? dateList.map((date, i) => {
              return (
                <RenderDate
                  date={date}
                  key={`RenderDate_${i}`}
                  currentEvent={currentEvent}
                  timeFormat={timeFormat}
                />
              );
            })
          : ""}
      </div>
    </>
  );
}

export const venueFactory = (currentEvent) => {
  if (!currentEvent) {
    return {};
  }
  if (currentEvent.venue) {
    return { ...currentEvent };
  }
  return {
    ...currentEvent,
    venue: {
      name: currentEvent.venueName,
      address: currentEvent.venueAddress,
      postalCode: currentEvent.venuePostCode,
      locality: currentEvent.venueCity,
      country: currentEvent.venueCountry,
    },
  };
};

export const ShowImage = () => {
  const { currentEvent, eventsConfig } = useContext(EventContext);
  const images = [1, 2, 3];
  return (
    <>
      {images
        .filter((imageIndex) => {
          return (
            !!eventsConfig[`singleEventShowImage${imageIndex}`] &&
            !!currentEvent[`image${imageIndex}`]
          );
        })
        .map((imageIndex) => {
          return (
            <img
              key={`show_image_${imageIndex}`}
              src={`https://events.brahmakumaris.org${currentEvent[`image${imageIndex}`]}`}
              className="img-fluid"
              alt={currentEvent.name}
            />
          );
        })}
    </>
  );
};

function CalendarFirstDate({ dateList, currentEvent, timeFormat }) {
  return (
    <div className="card card-body bg-light mb-2 calendar-first-date">
      {Array.isArray(dateList) &&
        dateList.length > 0 &&
        dateList.slice(0, 1).map((date, i) => {
          return (
            <RenderDate
              date={date}
              key={`RenderDate_${i}`}
              currentEvent={currentEvent}
              timeFormat={timeFormat}
            />
          );
        })}
    </div>
  );
}

/**
 * Displays the extended content about an event.
 * @returns {*}
 * @constructor
 */
export const ReadMore = ({ dateList: injectDateList }) => {
  const { currentEvent } = useContext(EventContext);
  const dateList = currentEvent?.dateList ?? injectDateList;
  const intermediateDateList = currentEvent?.intermediateDateList ?? [];
  const venueEvent = venueFactory(currentEvent);
  const timeFormat = useTimeFormat();

  const tags = convertTagsToSet(currentEvent);
  if (venueEvent?.venue) {
    return (
      <>
        {tags.has(TAGS.HIDE_TITLE) ? (
          ""
        ) : (
          <h2 id="eventDisplayName">{venueEvent.name}</h2>
        )}
        {tags.has(TAGS.HIDE_SUBTITLE) ? (
          ""
        ) : (
          <h3 id="eventSubTitle">{venueEvent.subTitle}</h3>
        )}
        <div className="row">
          <div
            className={
              venueEvent.requiresRegistration ? "col-md-6" : "col-md-12"
            }
          >
            {tags.has(TAGS.HIDE_TYPE) ? (
              ""
            ) : (
              <EventType eventTypeInt={venueEvent.eventTypeId} />
            )}
            {tags.has(TAGS.HIDE_DATE) ? (
              ""
            ) : (
              <CalendarFirstDate
                dateList={dateList}
                currentEvent={currentEvent}
                timeFormat={timeFormat}
              />
            )}
            {tags.has(TAGS.HIDE_IMAGE) ? "" : <ShowImage />}
            {tags.has(TAGS.HIDE_DESCRIPTION) ? (
              ""
            ) : (
              <p dangerouslySetInnerHTML={{ __html: venueEvent.description }} />
            )}
            {tags.has("hide-webcast") ? (
              ""
            ) : (
              <div className="row">
                <div className="col-md-12 webcastButton">
                  <WebcastButton original={venueEvent} />
                </div>
              </div>
            )}

            {tags.has("hide-venue") ? (
              ""
            ) : (
              <Venue
                venue={venueEvent.venue}
                venueName={venueEvent.venue.name}
                venueAddress={venueEvent.venue.address}
                venuePostalCode={venueEvent.venue.postalCode}
                venueLocality={venueEvent.venue.locality}
                currentEvent={currentEvent}
              />
            )}
            {!tags.has("hide-upcoming") && dateList && (
              <RenderUpcomingDates
                dateList={dateList}
                currentEvent={venueEvent}
              />
            )}
            {!tags.has("hide-intermediate") && dateList && (
              <RenderUpcomingDates
                dateList={intermediateDateList}
                currentEvent={venueEvent}
                isIntermediate={true}
              />
            )}
            {tags.has("hide-email") ? (
              ""
            ) : (
              <ContactEmail currentEvent={venueEvent} />
            )}
            {tags.has("hide-social") ? (
              ""
            ) : (
              <SocialIcons currentEvent={currentEvent} />
            )}
            {tags.has("hide-similar") ? "" : <RenderSimilarEvents />}
          </div>
          {tags.has("hide-form") ? (
            ""
          ) : (
            <IncludeForm currentEvent={venueEvent} />
          )}
        </div>
      </>
    );
  } else {
    return <></>;
  }
};

export const ContactEmail = ({ currentEvent }) => {
  const { t } = useTranslation();
  const contactEmail = currentEvent.contactEmail;
  if (!!contactEmail) {
    return (
      <div className="contact-email">
        <h4 className="card-title mt-2">{t("Contact Email")}</h4>
        <div className="card mt-2">
          <div className="card-body bg-light">
            <p className="card-text">
              &#128231; <a href={`mailto:${contactEmail}`}>{contactEmail}</a>
            </p>
          </div>
        </div>
      </div>
    );
  }
  return <></>;
};

const ReadMoreModal = makeModal(ReadMore);

export default ReadMoreModal;
