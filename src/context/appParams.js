export const DISPLAY_ORG_FILTER = "displayOrgFilter";
export const DISPLAY_ONLINE_FILTER = "displayOnlineFilter";
export const GOOGLE_MAPS_API_KEY = "AIzaSyDoI19uTwHav_Ptyd3ozjHWiGXN1cKZC2Y";
export const EVENTS_LIMIT = 2000;

export const URL_PARAMS = {
  EVENT_SESSION_ID: "eventSessionId",
};

export const EVENT_CONFIG = {
  SINGLE_EVENT_URL_TEMPLATE: "singleEventUrlTemplate",
  SPECIAL_IPAD_TEMPLATE: "specialIpadLink",
  EVENTS_LIMIT: "eventsLimit",
  HIDE_SEARCH: "hideSearch",
  HIDE_PAGER: "hidePager",
  TAGS: "tags",
  NO_TAGS: "noTags",
  EVENT_TYPE_IDS: "eventTypeIds",
  INITIAL_PAGE_SIZE: "initialPageSize",
  EVENT_DATE_FORMAT: "dateFormat",
};

export const LINK_NAME_FUNC = "eventsCalendarFunction";

export const TAGS = {
  HIDE_SUBTITLE: "hide-subtitle",
  HIDE_TYPE: "hide-type",
  HIDE_TITLE: "hide-title",
  HIDE_DATE: "hide-date",
  HIDE_IMAGE: "hide-image",
  SHOW_IMAGE_1: "show-image1",
  SHOW_IMAGE_2: "show-image2",
  SHOW_IMAGE_3: "show-image3",
  HIDE_DESCRIPTION: "hide-description",
  HIDE_WEBCAST: "hide-webcast",
  US_DATE: "us-date",
};

export const COMPOSITE_CALENDAR_PARAMS = {
  SHOW_WEEK: "compositeCalendar_showWeek",
  SHOW_DAY: "compositeCalendar_showDay",
  SHOW_IMAGE_CARDS: "compositeCalendar_showImageCards",
};
