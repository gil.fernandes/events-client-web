export const ONLINE_STATUSES = {
  NONE: "None",
  ONLINE_ONLY: "Online",
  IN_PERSON: "In Person",
  MIXED: "Mixed",
  HAS_WEBCAST: "HasWebcast",
};

export const ONLINE_STATES = Object.values(ONLINE_STATUSES);
