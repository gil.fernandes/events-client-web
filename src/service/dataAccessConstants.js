export const DATA_ACCESS_PARAMS = {
  LOGICAL_YES: "yes",
  LOGICAL_NO: "no",
  ONLINE_ONLY: "onlineOnly",
  ONLY_WEBCAST: "onlyWebcast",
};

export const eventMap = {
  1: "Lecture",
  2: "Seminar",
  3: "Course",
  4: "Conference",
  5: "Retreat",
  6: "Workshop",
  7: "Panel discussion",
  8: "Special event",
  9: "Meditation",
  10: "Miscellaneous",
  17: "Festivals",
};

export const QUERY_PARAMS = {
  START_DATE_LIMIT: "startDateLimit",
  NO_TAGS: "noTags",
};

export const TAGS_OPERATOR = {
  AND: "AND",
};
